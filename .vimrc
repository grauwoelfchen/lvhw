set nocompatible
set nobackup
set noswapfile
set showmode
set showcmd
set autoread
set ttyfast
set vb t_vb=
set hidden

set noimcmdline
set iminsert=0

set splitbelow
set splitright

set autoindent
syntax enable

let g:mapleader = ","
let g:maplocalleader = "\\"

set wildignore+=.git/*

noremap ; :
noremap : ;

inoremap hl <Esc>
inoremap jk <Esc>
inoremap <Esc> <Nop>

filetype off
"" vim-plug
call plug#begin(expand($HOME.'/.vim/plugged'))
Plug 'Shougo/vimproc.vim'
Plug 'thinca/vim-quickrun'
call plug#end()
filetype plugin indent on

"" quickrun
let g:quickrun_config = {}
let g:quickrun_config['_'] = {
\  'hook/time/enable': 0,
\  'runner': 'vimproc',
\  'runner/vimproc/updatetime': 10,
\  'split': 'vertical'
\}
