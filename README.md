# LVHW

Exercises from [Learn Vimscript the Hard Way](
http://learnvimscriptthehardway.stevelosh.com/).


## Used plugins

* vim-plug
* vimproc
* vim-quickrun

```zsh
% vim -u .vimrc src/chapter-NN.vim
```


## Links

* http://learnvimscriptthehardway.stevelosh.com/
